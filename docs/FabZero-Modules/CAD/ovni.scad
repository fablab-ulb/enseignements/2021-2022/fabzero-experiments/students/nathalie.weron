/*
FILE   : ovni.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-02-26
MODIFIED: 2022-04-02
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn=50; // definition of roundness
h = 8; // height of the flexlinks
d = 90; // length of the middle (do not include the extremities)
w = 1; // thickness of the middle
rc=4; // radius for the extremities
e=10; // ditance between the holes centres
hole = 3; // radius of the holes
        

difference(){ //first, to divide bodies and holes
    
    union(){// all the bodies
        
        //middle_body
        translate([0,-(3*e/2),0]){//put at centre
            cylinder(h, r=rc);
            translate([0,3*e,0])
            cylinder(h, r=rc);
            translate([-rc,0,0])
            cube([2*rc,3*e,h]);
        }
        
        //extremity1_body: left
        translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
            rotate([0,0,45]){
                translate([-rc,-2*e,0])
                cube([2*rc,2*e,h]);
                translate([0,-2*e,0])
                cylinder(h,r=rc);
            }
        }
        
        //extremity2_body: rigth
        translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
            rotate([0,0,-45]){
                translate([-rc,0,0])
                cube([2*rc,2*e,h]);
                translate([0,2*e,0])
                cylinder(h,r=rc);
            }
        }
    }
    
    //HOLES
    //middle_holes
    translate([0,-(e+e/2),0]){ //put at centre
        cylinder(h, r=hole); //left hole
        translate([0,e,0]) //middle hole
        cylinder(h, r=hole); 
        translate([0,2*e,0])
        cylinder(h, r=hole);
        translate([-hole,e,0])
        cube([2*hole,e,h]);
        translate([0,3*e,0])  // right hole
        cylinder(h, r=hole);
        
    }
    
    //extremity1_holes: left
    translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
        rotate([0,0,45]){
            translate([0,-e,0]) //second hole
            cylinder(h,r=hole); 
            translate([0,-2*e,0]) //end hole
            cylinder(h,r=hole);
        }
    }
 
    //extremity2_holes
    translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
        rotate([0,0,-45]){
            translate([0,e,0]) //second hole
            cylinder(h,r=hole);
            translate([0,2*e,0]) //end hole
            cylinder(h,r=hole);
        }
    }
        
}