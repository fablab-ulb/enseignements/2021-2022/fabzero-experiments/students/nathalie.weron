/*
FILE   : ovni_final.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-02-25
MODIFIED : 2022-03-24
MODIFIED : 2022-04-02
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)..

//*/

$fn=50; //definition of roundness
h = 8; // heigth of the flexlink
d = 90; // length of the middle (do not include the extremities)
w = 1; // thickness of the middle
rc=4; // radius for the extremities
e=8; // distance between the holes centres
hole = 2.5; // radius of the holes
//size of extremities = e + 2*rc
// total lenght = d + 2*size of extremities

difference(){ //first, to divide bodies and holes
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])
        cube([w,d,h]);

        //extremity 1: top
        cylinder(h, r=rc);
        translate([0,e,0])
        cylinder(h, r=rc);
        translate([-rc,0,0])
        cube([2*rc,e,h]);

        //extremity 2: bottom 
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])
            cylinder(h, r=rc);
            translate([-rc,0,0])
            cube([2*rc,e,h]);
        }

        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])
                cylinder(h, r=rc);
                translate([-rc,0,0])
                cube([2*rc,3*e,h]);
            }

            //extremity1_body: left
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([-rc,-2*e,0])
                    cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])
                    cylinder(h,r=rc);
                }
            }

            //extremity2_body: right
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){
                    translate([-rc,0,0])
                    cube([2*rc,2*e,h]);
                    translate([0,2*e,0])
                    cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES

    //extremity 1_holes: top
    cylinder(h, r=hole); //hole 1
    translate([0,e,0]) // hole 2
    cylinder(h, r=hole);

    //extremity 2_holes: bottom
    translate([0,d+(2*rc+e),0]){
        cylinder(h, r=hole); //hole 1
        translate([0,e,0]) // hole 2 
        cylinder(h, r=hole);
    }

    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),0]){ //put at centre
                cylinder(h, r=hole); //left hole
                translate([0,e,0]) // middle hole
                cylinder(h, r=hole);
                translate([0,2*e,0])
                cylinder(h, r=hole);
                translate([-hole,e,0])
                cube([2*hole,e,h]);
                translate([0,3*e,0]) // right hole
                cylinder(h, r=hole);
                
            }

            //extremity1_holes: left
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([0,-e,0]) //second hole
                    cylinder(h,r=hole);
                    translate([0,-2*e,0]) //end hole
                    cylinder(h,r=hole);
                }
            }

            //extremity2_holes: right
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){
                    translate([0,e,0]) //second hole
                    cylinder(h,r=hole);
                    translate([0,2*e,0]) //end hole
                    cylinder(h,r=hole);
                }
            }
        }
}