
/*
   FILE   : test_thermometer

   AUTHOR : Nathalie Wéron

   DATE   : 2022-03-17

   BASED ON :
    A code created by ArduinoGetStarted.com
    This example code is in the public domain
    Tutorial page: https://arduinogetstarted.com/tutorials/arduino-lm35-temperature-sensor
 */

#define ADC_VREF_mV    5000.0 // in millivolt
#define ADC_RESOLUTION 1024.0
#define PIN_LM35       A0


void setup() {
  Serial.begin(9600);
}

void loop() {
  // get the ADC value from the temperature sensor by using analogRead() function. 
  int adcVal = analogRead(PIN_LM35);
  // convert the ADC value to voltage in millivolt
  float milliVolt = adcVal * (ADC_VREF_mV / ADC_RESOLUTION);
  // convert the voltage to the temperature in Celsius
  float tempC = milliVolt / 10;

  // print the temperature in the Serial Monitor:
  Serial.print("Temperature: ");
  Serial.print(tempC);   // print the temperature in Celsius
  Serial.println("°C");  // println to say it is the end of the line
 
  delay(1000);
}
