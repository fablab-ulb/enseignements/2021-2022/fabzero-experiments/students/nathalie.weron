/* FILE   : sound_thermometer
    
    AUTHOR : Nathalie Wéron
    
    DATE   : 2022-03-17
    
    LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
    
    BASED ON : 
      For the thermometer: 
        The code created by ArduinoGetStarted.com
        This example code is in the public domain
        Tutorial page: https://arduinogetstarted.com/tutorials/arduino-lm35-temperature-sensor
  
      For the buzzer: 
        The toneMelody in the Digital Examples in File.
        Created 21 Jan 2010
        Modified 30 Aug 2011
        By Tom Igoe
        This example code is in the public domain.
       Tutorial page: https://arduinogetstarted.com/tutorials/arduino-piezo-buzzer
 */


/*Thermometer*/
#define ADC_VREF_mV    5000.0 // in millivolt
#define ADC_RESOLUTION 1024.0
#define PIN_LM35       A0
int tobj = 23;  //23°C is the test temperature that was easily achieved in the class.

/*Song*/
#include "pitches.h"

// notes in the melody: based on the opening of chest of The Legend of Zelda: Ocarina of Time but failed.
int melody[] = {
  NOTE_E5, NOTE_E5, NOTE_D5, NOTE_F5, NOTE_E5
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  8, 8, 8, 8, 8,
};


void setup() {
  Serial.begin(9600);
}

void loop() {
  // get the ADC value from the temperature sensor by using analogRead() function. 
  int adcVal = analogRead(PIN_LM35);
  // convert the ADC value to voltage in millivolt
  float milliVolt = adcVal * (ADC_VREF_mV / ADC_RESOLUTION);
  // convert the voltage to the temperature in Celsius
  float tempC = milliVolt / 10;

  // print the temperature in the Serial Monitor:
  Serial.print("Temperature: ");
  Serial.print(tempC);   // print the temperature in Celsius
  Serial.println("°C");  // println to say it is the end of the line

  if (tempC > tobj) {
  // iterate over the notes of the melody:
  int size = sizeof (noteDurations) / sizeof(int);
  
  for (int thisNote = 0; thisNote < size; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(8, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
  }
  }
  
 delay(1000);
}
