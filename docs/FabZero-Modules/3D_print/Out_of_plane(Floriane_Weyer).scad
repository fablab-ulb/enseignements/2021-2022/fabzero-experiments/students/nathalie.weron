/*
Fichier : Out_of_Plane.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

$fn=50;
// Paramètres

// Hauteur de la tête
height = 6.4;

// Nombre de trous
n = 2;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Rayon de courbure interne
beam_radius = 30;
// Epaisseur de la tige
beam_thickness = 1.5;

//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
translate([0,(height/2),radius+edge_thickness])
rotate([90,0,0])    
head();

// Création de la deuxième tête    
translate([0, (height/2)+(2*beam_radius)+(beam_thickness),radius+edge_thickness])
rotate([90,0,0])
head();

// Création de la tige entre les deux têtes   
translate([-radius, beam_radius+(beam_thickness/2),0])
difference(){
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius + beam_thickness, center = True);
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius, center = True);
    translate([0,-(2*(beam_radius+beam_thickness)),0])
    cube([(2*(beam_radius+beam_thickness)), (4*(beam_radius+beam_thickness)), 2*(radius+edge_thickness)]);
    }