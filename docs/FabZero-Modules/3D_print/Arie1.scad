$fn = 150; //défini la précision des courbes

// paramètres du Flexstick à tige cylindrique : 

radius = 2.45; //rayon des trous
height = 9.6; // Hauteur de la pièce
distance = 8; // Distance centre à centre entre deux trous
thickness = 0.7; // Distance entre l'extérieur du trou et l'extérieur de la pièce
size_flex = 55; // longueur de la tige cylindrique 
side_flex = 1; // largeur de la tige cylindrique
N_holes=2;  //numeros de trous pour chaque partie
 lenght=N_holes*8;//longeur des deux blocs, modifiée pour permettre d'avoir une longeur proportionné de la piece.


// Code de la tige :

rotate([90,0,90])translate([0,0,-30])cylinder(h = size_flex, r1 = side_flex, r2 = side_flex, center = true);

//code des embouts 

module sticks(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+thickness, center = true);
            translate([(N_holes-1)*distance,0,0]){cylinder(h = height, r = radius+thickness, center = true);}
            }
        for (i = [1:N_holes]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = true);
                }
            }
        }
    }
//Première attache 
   sticks();

// Deuxième attache
translate([-((N_holes-1)*distance)-(2*radius)-(2*thickness)-(size_flex-5),0,0]){
    sticks();
    } 