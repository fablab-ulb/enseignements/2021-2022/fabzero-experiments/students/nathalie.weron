/* 
FILE   : cylinders_test.scad

AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-28
MODIFIED: 2022-03-31
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn=50; // definition of roundness
h = 3.5; // height
rc=3.3; // radius exterior
e=8; // space between the cylinders' centres
cyl = 2.5; // basic radius of the cylinders = the thing you want to try
test = 0.02 ; // added to the radius of the cylinders, choose what you want
N_test = 4 ; // number of test you want to do. If you want more you have to add more cylinders at the end of the code.
hc = h+1.7; // height for the cylinders

//Code:
//Base
    translate([-rc,-rc,0])
        cube([2*rc,2*rc,h]); // to recognise which side is which
    translate([0,N_test*e,0])
    cylinder(h, r=rc);
    translate([-rc,0,0])
    cube([2*rc,N_test*e,h]);
    
//Cylinders test
    cylinder(hc, r=cyl);
    translate([0,1*e,0])
    cylinder(hc, r=cyl+test);
    translate([0,2*e,0])
    cylinder(hc, r=cyl+2*test);
    translate([0,3*e,0])
    cylinder(hc, r=cyl-test);
    translate([0,4*e,0])
    cylinder(hc, r=cyl-2*test);