/* 
FILE   : holes_test.scad

AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-28
MODIFIED: 2022-03-31
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn=50; // definition of roundness
h = 3.5; // height
rc=3.3; // radius exterior
e=8; // space between the holes' centres
hole = 2.5; // basic radius of the holes = the thing you want to try, 2.5 seems a bit tight
test = 0.02 ; // added to the radius of the holes, choose what you want
N_test = 3 ; // number of test you want to do. If you want more you have to add more holes at the end of the code.

//Code for 3 test and the basic radius
difference(){
    union(){
        translate([-rc,-rc,0])
        cube([2*rc,2*rc,h]); // to recognise which side is which
        translate([0,N_test*e,0])
        cylinder(h, r=rc);
        translate([-rc,0,0])
        cube([2*rc,N_test*e,h]);
    }
    cylinder(h, r=hole);
    translate([0,1*e,0])
    cylinder(h, r=hole+test);
    translate([0,2*e,0])
    cylinder(h, r=hole+2*test);
    translate([0,3*e,0])
    cylinder(h, r=hole+3*test);
}