/*
FILE   : ovni2.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-24
MODIFIED : 2022-03-26
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).


Basé sur le travail de Arié Nabet

//*/


$fn = 150; // definition of the roundness

//Parameters of the ovni flexlink 
radius = 2.5; // radius of the holes
height = 3.5; // heigth of the extremities and middle part
distance = 8; // distance between the holes centres 
thickness = 0.7; // distance between the holes and the exterior
size_flex = 100; // length of the cylinder middle     
side_flex = 0.75; // width of the cylinder middle 
N_holes=2;  // number of holes in one extremity 
 lenght=N_holes*8;// length of the extremities (modifiée pour permettre d'avoir une longeur proportionné de la piece)
 
//Parameters of the little cylinder at the extremity
h = 5; // the height of the cylinder
r1 = height/2 ; // the radius of the cylinder 

//Parameter of the middle ovni
rc=radius + thickness; // Radius of the body


//CODE
//Cylinder middle
rotate([90,0,90])translate([0,0,-(size_flex/2+radius+thickness)])cylinder(h = size_flex, r = side_flex, center = true);

//Stick module
module stick(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+thickness, center = true);
            translate([(N_holes-1)*distance,0,0]){cylinder(h = height, r = radius+thickness, center = true);}
            }
        for (i = [1:N_holes]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = true);
                }
            }
        }
    }
//extremity 1
   stick();

// extremity 2: little cylinder
translate([-(h+rc+size_flex),0,0])
    rotate([0,90,0])
    cylinder(h, r = r1);
    
// ovni
translate([-(rc+size_flex/2),0,-height/2])
difference(){ //first, to divide bodies and holes
    
    union(){// all the bodies
        //middle
        translate([0,-(3*distance/2),0]){//put at centre
            cylinder(height, r=rc);
            translate([0,3*distance,0])
            cylinder(height, r=rc);
            translate([-rc,0,0])
            cube([2*rc,3*distance,height]);
        }
        
        //extremity1_body: right
        translate([0,-(distance+distance/2),0]){ //fix the futur centre at the origin before the rotation
            rotate([0,0,45]){
                translate([-rc,-2*distance,0])
                cube([2*rc,2*distance,height]);
                translate([0,-2*distance,0])
                cylinder(height,r=rc);
            }
        }
        
        //extremity2_body: left
        translate([0,distance+distance/2,0]){ //fix the futur centre at the origin before the rotation
            rotate([0,0,-45]){
                translate([-rc,0,0])
                cube([2*rc,2*distance,height]);
                translate([0,2*distance,0])
                cylinder(height,r=rc);
            }
        }
    }
    
    //HOLES
    //middle_holes
    translate([0,-(distance+distance/2),0]){ //put at centre
        cylinder(height, r=radius); // right hole
        translate([0,distance,0]) // middle hole
        cylinder(height, r=radius);
        translate([0,2*distance,0])
        cylinder(height, r=radius);
        translate([0,3*distance,0]) // left hole
        cylinder(height, r=radius);
        
    }
    
    //extremity1_holes: right
    translate([0,-(distance+distance/2),0]){ //fix the futur centre at the origin before the rotation
        rotate([0,0,45]){
            translate([0,-distance,0]) //second hole
            cylinder(height,r=radius);
            translate([0,-2*distance,0]) //end hole
            cylinder(height,r=radius);
        }
    }
 
    //extremity2_holes: left
    translate([0,distance+distance/2,0]){ //fix the futur centre at the origin before the rotation
        rotate([0,0,-45]){ 
            translate([0,distance,0]) //second hole
            cylinder(height,r=radius);
            translate([0,2*distance,0]) //end hole
            cylinder(height,r=radius);
        }
    }
        
} 
    