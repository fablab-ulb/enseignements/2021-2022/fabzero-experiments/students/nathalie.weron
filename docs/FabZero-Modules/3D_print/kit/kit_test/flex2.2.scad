/* 
FILE   : flex2.2.scad

AUTHOR : Nathalie Wéron

DATE   : 2022-03-28
MODIFIED: 2022-03-29

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn=50; //definition of roundness
h = 3.5; // height of the flexlink
d = 50; // length of the middle (do not include the extremities)
w = 1; // thickness of the middle
rc=3.3; // radius for the extremities
e=8; // ditance between the holes centres
hole = 2.5; // radius of the holes
//size of extremities = e + 2*rc
// total lenght = d + 2*size of extremities

// middle
translate([-w/2,e+rc,0])
cube([w,d,h]);

//extremity 1
difference(){
    union(){ //body
        cylinder(h, r=rc);
        translate([0,e,0])
        cylinder(h, r=rc);
        translate([-rc,0,0])
        cube([2*rc,e,h]);
    }
    cylinder(h, r=hole); // hole 1
    translate([0,e,0])
    cylinder(h, r=hole); // hole 2
}

//extremity 2
difference(){
    union(){ //body
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])
            cylinder(h, r=rc);
            translate([-rc,0,0])
            cube([2*rc,e,h]);
        }
    }
    translate([0,d+(2*rc+e),0]){
        cylinder(h, r=hole); // hole 1
        translate([0,e,0])
        cylinder(h, r=hole); // hole 2
    }

}