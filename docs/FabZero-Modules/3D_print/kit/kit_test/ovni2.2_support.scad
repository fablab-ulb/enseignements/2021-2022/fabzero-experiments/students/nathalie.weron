/*
FILE   : ovni2.2_support.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-29
    
LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).


Basé sur le travail de Arié Nabet

//*/


$fn = 150; //défini la précision des courbes

// paramètres du Flexstick à tige cylindrique : 

radius = 2.52; //rayon des trous
height = 3.5; // Hauteur de la pièce
distance = 8; // Distance centre à centre entre deux trous
thickness = 0.7; // Distance entre l'extérieur du trou et l'extérieur de la pièce
size_flex = 58; // longueur de la tige cylindrique 
side_flex = 0.75; // largeur de la tige cylindrique
N_holes=2;  //numeros de trous pour chaque partie
 lenght=N_holes*8;//longeur des deux blocs, modifiée pour permettre d'avoir une longeur proportionné de la piece.
 
//Parameters of the little cylinder at the extremity
h = 5; // the height
r1 = height/2 ; // the radius

//Parameters of the middle ovni
rc=radius + thickness;

//CODE:
    union(){//SHADOW
            //extremity 2: little cylinder
            rotate([0,0,90]){
            difference(){
            translate([-height/2-0.5,rc+size_flex,-height/2])
                    cube([2*r1+1,h,height/2]);
            translate([0,rc+size_flex+h,0]) //maybe a Z translation of 0.5 would work
            rotate([90,0,0])
                cylinder(h, r = r1+0.3); //maybe 0.5 like ovni4_support would work
            }
        }
    }

// Code de la tige :

rotate([90,0,90])translate([0,0,-(size_flex/2+radius+thickness)])cylinder(h = size_flex, r = side_flex, center = true);

//code des embouts 

module sticks(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+thickness, center = true);
            translate([(N_holes-1)*distance,0,0]){cylinder(h = height, r = radius+thickness, center = true);}
            }
        for (i = [1:N_holes]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = true);
                }
            }
        }
    }
//Première attache 
   sticks();

// Deuxième attache: little cylinder
translate([-(h+rc+size_flex),0,0])
    rotate([0,90,0])
    cylinder(h, r = r1);
    
// ovni middle
translate([-((size_flex/2)+rc),0,-height/2])
difference(){ //first, to divide bodies and holes
    
    union(){// all the bodies
        
        //middle_body
        translate([0,-(3*distance/2),0]){//put at centre
            cylinder(height, r=rc);
            translate([0,3*distance,0])
            cylinder(height, r=rc);
            translate([-rc,0,0])
            cube([2*rc,3*distance,height]);
        }
        
        //extremity1_body
        translate([0,-(distance+distance/2),0]){
            rotate([0,0,45]){
                //fix the futur centre at the origin before the rotation
                translate([-rc,-2*distance,0])
                cube([2*rc,2*distance,height]);
                translate([0,-2*distance,0])
                cylinder(height,r=rc);
            }
        }
        
        //extremity2_body
        translate([0,distance+distance/2,0]){
            rotate([0,0,-45]){
                //fix the futur centre at the origin before the rotation
                translate([-rc,0,0])
                cube([2*rc,2*distance,height]);
                translate([0,2*distance,0])
                cylinder(height,r=rc);
            }
        }
    }
    
    //HOLES
    //middle_holes
    translate([0,-(distance+distance/2),0]){ //put at centre
        cylinder(height, r=radius);
        translate([0,distance,0])
        cylinder(height, r=radius);
        translate([0,2*distance,0])
        cylinder(height, r=radius);
        translate([0,3*distance,0])
        cylinder(height, r=radius);
        
    }
    
    //extremity1_holes
    translate([0,-(distance+distance/2),0]){//same as body
        rotate([0,0,45]){//same as body
            translate([0,-distance,0])
            cylinder(height,r=radius);//same holes cylinders, height+2, r=hole
            translate([0,-2*distance,0])
            cylinder(height,r=radius);
        }
    }
 
    //extremity2_holes
    translate([0,distance+distance/2,0]){ //same as body
        rotate([0,0,-45]){ //same as body
            translate([0,distance,0])
            cylinder(height,r=radius);//same holes cylinders, height+2, r=hole
            translate([0,2*distance,0])
            cylinder(height,r=radius);
        }
    }
        
} 