/*
FILE   : out_of_plane_test_thickness.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-24
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Basé sur Out_of_Plane.scad de Floriane Weyer
Date : 02/03/21
Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

$fn=50; //definition of roundness

height = 3.5; // heigth of the extremities

n = 2; // number of holes in one extremity
radius = 2.52; // radius of the holes
distance = 8; // distance between the holes centres 
edge_thickness = 0.7; //Distance between the holes and the exterior

beam_radius = 50; // interior radius of the beam   
beam_thickness = 1; // thickness of the beam  

//Definition of the head module 
module head(){
    difference(){
        union(){
            translate([-radius+1, 0, height/2]) {cube([radius, 2*radius + 2*edge_thickness , beam_thickness], center = true);}
            hull(){
                cylinder(h = height, r = radius+edge_thickness);
                
                translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness);}
                }
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius);
                }
            }
        }
    }
// First extremity
translate([0,(height/2),radius+edge_thickness])
rotate([90,0,0])    
head();

// Second extremity   
translate([0, (height/2)+(2*beam_radius)+(beam_thickness),radius+edge_thickness])
rotate([90,0,0])
head();

// Beam  
translate([-radius, beam_radius+(beam_thickness/2),0])
difference(){
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius + beam_thickness);
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius);
    translate([0,-(2*(beam_radius+beam_thickness)),0])
    cube([(2*(beam_radius+beam_thickness)), (4*(beam_radius+beam_thickness)), 2*(radius+edge_thickness)]);
    }