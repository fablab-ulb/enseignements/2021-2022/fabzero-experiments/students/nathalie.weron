/*
FILE   : ovni3.2.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-03-25
MODIFIED: 2022-03-26
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

//*/

$fn = 50; // definition of the roundness
h = 3.5; // heigth of the flexlink
d = 58; // length of the middle (do not include the extremities)
w = 1; // thickness and width of the middle
rc=3.3; // radius for the extremities
e=8; // distance between the holes centres
hole = 2.55; // radius of the holes

//Parameters of the little cylinder at the extremity
h2 = 5; // the height of the cylinder
r1 = h/2 ; // the radius of the cylinder

//size of extremity 1 = e + 2*rc
// total lenght = d + size of extremity 1 + h2

//Code:
difference(){ //first, to divide bodies and holes
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,h/2-w/2])
        cube([w,d,w]);
        
        //extremity 1: top
        cylinder(h, r=rc);
        translate([0,e,0])
        cylinder(h, r=rc);
        translate([-rc,0,0])
        cube([2*rc,e,h]);
        
        //extremity 2: little cylinder
        translate([0,e+rc+d+h2,r1])
            rotate([90,0,0])
                cylinder(h2, r = r1);
        
        //OVNI
        translate([0,e+rc+(d/2),0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])
                cylinder(h, r=rc);
                translate([-rc,0,0])
                cube([2*rc,3*e,h]);
            }
            
            //extremity1_body: left
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([-rc,-2*e,0])
                    cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])
                    cylinder(h,r=rc);
                }
            }
            
            //extremity2_body: right
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){
                    translate([-rc,0,0])
                    cube([2*rc,2*e,h]);
                    translate([0,2*e,0])
                    cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES
    
    //extremity 1_holes: top
    cylinder(h, r=hole);
    translate([0,e,0])
    cylinder(h, r=hole);
    
    
    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),0]){ //put at centre
                cylinder(h, r=hole); // left hole
                translate([0,e,0]) //middle hole
                cylinder(h, r=hole);
                translate([0,2*e,0])
                cylinder(h, r=hole);
                translate([0,3*e,0]) //right hole
                cylinder(h, r=hole);
            }
            
            //extremity1_holes: right
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([0,-e,0]) //second hole
                    cylinder(h,r=hole);
                    translate([0,-2*e,0]) //end hole
                    cylinder(h,r=hole);
                }
            }
         
            //extremity2_holes
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){ 
                    translate([0,e,0]) // second hole
                    cylinder(h,r=hole);
                    translate([0,2*e,0]) // end hole
                    cylinder(h,r=hole);
                }
            }
        }
}