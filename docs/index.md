# Welcome !
I hope you are alright.
Sit tight because you are about to read the wonderful presentation of a nice and very humble person that started the "How To Make (almost) Any Experiments / FabZero inside " experience in February 2022.                         
If you are not aware, this title belongs to a course proposed by the Free University of Brussels (_Université libre de Bruxelles_, ULB in short) in its own FabLab, a place holding a multitude of digital fabrication tools that you will discover, with me, on the other pages of this website, in the FabZero Modules tab.

![FabLab ULB logo](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/images/logo-FablabULB.jpg)


# More about me
Hello, nice to meet you !

![My beautiful self: brown hair, glasses and Ravenclaw scarf](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/images/newme2.JPG)

My name is **Nathalie Wéron**. I am a Belgian student, currently in **Master 2 of Bioengineering in
Agronomics Science** at the ULB. I am 22 years old and my birthday is the 16th of April. I do not really
care about presents but I do like desserts (with apples, caramel, a bit of chocolate ... yummy !).


## Formation
I started my University course in 2017 with a Bachelor in Bioengineering at the ULB. I chose this study because I did not know
what to do in science and bioengineering is a good mix of science's branches that leads to vast working opportunities.
There, I discovered the fascinating world of modern agriculture that combines economics, social, chemistry, physics and biology.
I grew a passion for **sustainable agriculture**. For me, it includes agroecology and a lot of practices
like agroforestry, regenerative agriculture, feed autonomy and a lot more.
For this reason, I started the Bioengineering Master in Agronomics Science. During my first year,
I did an **Erasmus to Córdoba, in Spain**. I learned Spanish to follow my courses and live my everyday life there.

You can easily read my [**linkedin page** here](https://www.linkedin.com/in/nathalie-w%C3%A9ron-132853201/)
for more information.


### But why did I choose this course ?
In the second year of my masters, all the courses had to be chosen from a list.
At first, I did not want to take this one as a lot of the other courses interested me. Nevertheless, I chose to
take classes that teach know-how or subjects that I would have difficulties to learn after my studies.
As you can read further, I love doing manual activities and what better course than this one to learn know-how about fabrication.
Furthermore, the mindset of **the Frugal Science movement** ([see the Professor's blog, in french](https://dtwg4.github.io/blog-flexible-jekyll/entrepreneuriat-social-et-science-frugale/)
or [this page from the University of Stanford, USA](https://www.frugalscience.org/)) speaks to me very much because
it combines low technology with world's problematics.
Also, I already took an engineering class (_Aplicaciones prácticas en ingenieria rural_) during my Erasmus and
it was both a challenging and rewarding experience that I loved.


## Hobbies

I must confess, usually I watch series (real actors or animation), movies (same) or
Youtube videos (for entertainment or science and history).
However, when I have time and ideas I love to sew, draw, cook snacks.

* I have already done my 2 school bags (a backpack and a messenger bag), some clothes' repairs and a cloak.

* I love to make my week's snacks.

I also have hundreds and hundreds of books including a lot of manga and French, English and Spanish novels.
I have books about cooking, sustainable lifestyles, biology and gardening.
I would love to have an aromatic and medicinal garden with a place for wild fauna and flora.

In my life, I did a lot of different sports but not really during a long time:

* Dances: jazz, oriental

* Martial arts: Jiu Jitsu, Kung Fu

* Climbing

* Horsing

* Fencing

* Parkour

## Random information    
* I regularly change the theme of the website because I cannot visualize them before I make the change and I want to have the best.  
    Now it is _flatly_.                    
    **Update**: I found [this website](https://mkdocs.readthedocs.io/en/0.11.1/user-guide/styling-your-docs/) that gives some theme previews thanks to Pauline's Documentation module.         

* There are differences between my _Markdown Preview_ from Atom and the final result on the website ...
