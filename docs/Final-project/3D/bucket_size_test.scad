/*
FILE   : bucket_size_test.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-06

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn = 200;
radius = 100; // radius of the bucket: top or bottom
little_height = 10; // height of the part entouring the bucket
thickness = 1;

//Code:
difference (){
        cylinder(little_height, r = radius+thickness); // bucket size
        cylinder(little_height, r = radius); // bucket size hole
}