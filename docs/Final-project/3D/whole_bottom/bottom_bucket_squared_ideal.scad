/*
FILE   : bottom_bucket_squared_ideal.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-05
MODIFIED : 2022-05-25

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn = 200;
bottom = 3; // thickness of the bottom
bottom_heigth = 50; // height of the bottom under the bucket
radius = 100; // radius of the bucket
little_heigth = 10; // height of the part entouring the bucket
neg_thickness = 2; // thickness of the sides
thickness = 1;
tap_hole = 20/2; // 20 mm the internal part of the tap that goes through the tap hole
tap_fixer = (30-20)/2; // 30 mm the external size of the tap screw thing
cyl_radius1 = radius/2; // radius of the rounded corner of the bottom piece
cyl_radius2 = radius/3.5; // coefficient to modify the bottom part: near 50 -> like a cube with rounded corner, near 0 -> like the top part has been rotated to 90 degrees
slope = 10; // slope in the bottom of the piece


// Code:
difference (){ 
    union (){
        hull () { //liquid location
            translate ([cyl_radius1,cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius1+thickness, r2 = cyl_radius2);
            translate ([cyl_radius1,-cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius1+thickness, r2 = cyl_radius2);
            translate ([-cyl_radius1,-cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius1+thickness, r2 = cyl_radius2);
            translate ([-cyl_radius1,cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius1+thickness, r2 = cyl_radius2);
            translate ([cyl_radius1,0,0])
            cylinder(bottom_heigth, r1 = cyl_radius2+thickness, r2 = cyl_radius1);
            translate ([0,cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius2+thickness, r2 = cyl_radius1);
            translate ([-cyl_radius1,0,0])
            cylinder(bottom_heigth, r1 = cyl_radius2+thickness, r2 = cyl_radius1);
            translate ([0,-cyl_radius1,0])
            cylinder(bottom_heigth, r1 = cyl_radius2+thickness, r2 = cyl_radius1);
        
        translate ([0,0,bottom_heigth]) // bucket size
            cylinder(little_heigth, r = radius+thickness);
        }

    }
    // HOLES
    translate ([0,0,bottom_heigth-(thickness + neg_thickness)]) // bucket size
            cylinder(little_heigth+(thickness + neg_thickness), r = radius);
    translate ([radius-neg_thickness,0,bottom+tap_hole+tap_fixer]) // tap hole
            rotate ([0,90,0])
                cylinder ((thickness + neg_thickness), r = tap_hole);
    hull () { // liquid location
            translate ([cyl_radius1,cyl_radius1,bottom])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2, r1 = cyl_radius1-neg_thickness, r2 = cyl_radius2-neg_thickness);
            translate ([cyl_radius1,-cyl_radius1,bottom])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2, r1 = cyl_radius1-neg_thickness, r2 = cyl_radius2-neg_thickness);
            translate ([-cyl_radius1,-cyl_radius1,bottom+slope])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2-slope, r1 = cyl_radius1-neg_thickness-slope, r2 = cyl_radius2-neg_thickness);
            translate ([-cyl_radius1,cyl_radius1,bottom+slope])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2-slope, r1 = cyl_radius1-neg_thickness-slope, r2 = cyl_radius2-neg_thickness);
            translate ([cyl_radius1,0,bottom])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2, r1 = cyl_radius2-neg_thickness, r2 = cyl_radius1-neg_thickness);
            translate ([0,cyl_radius1,bottom+slope])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2-slope, r1 = cyl_radius2-neg_thickness, r2 = cyl_radius1-neg_thickness);
            translate ([-cyl_radius1,0,bottom+slope])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2-slope, r1 = cyl_radius2-neg_thickness, r2 = cyl_radius1- neg_thickness);
            translate ([0,-cyl_radius1,bottom+slope])
            cylinder(bottom_heigth-(thickness + neg_thickness)*2-slope, r1 = cyl_radius2-neg_thickness, r2 = cyl_radius1-neg_thickness);
        }
}