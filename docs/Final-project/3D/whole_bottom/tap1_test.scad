/*
FILE   : tap1_test.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-06

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)..

//*/

$fn= 150;
neg_thickness = 2; // thickness of the sides
thickness = 1;
tap_hole = 21/2; // is okay for 20 mm the internal part of the tap that goes through the tap hole
tap_fixer = (30-20)/2; // 30 mm the external size of the tap screw thing


// Code: 
difference (){
    union () {
        translate ([(thickness + neg_thickness)/2,0,tap_hole+tap_fixer])
            cube ([(thickness + neg_thickness)*2, tap_hole*2+tap_fixer*2, 1], center = true);
        rotate ([90,0,0])
            translate ([(thickness + neg_thickness)/2,0,tap_hole+tap_fixer])
                cube ([(thickness + neg_thickness)*2, tap_hole*2+tap_fixer*2+1, 1], center = true);
        rotate ([180,0,0])
            translate ([(thickness + neg_thickness)/2,0,tap_hole+tap_fixer])
                cube ([(thickness + neg_thickness)*2, tap_hole*2+tap_fixer*2, 1], center = true);
        rotate ([-90,0,0])
            translate ([(thickness + neg_thickness)/2,0,tap_hole+tap_fixer])
                cube ([(thickness + neg_thickness)*2, tap_hole*2+tap_fixer*2+1, 1], center = true);
        cube ([thickness + neg_thickness, tap_hole*2+tap_fixer*2, tap_hole*2+tap_fixer*2], center = true);
    }
    rotate ([0,90,0])
        cylinder ((thickness + neg_thickness), r = tap_hole, center = true);
}