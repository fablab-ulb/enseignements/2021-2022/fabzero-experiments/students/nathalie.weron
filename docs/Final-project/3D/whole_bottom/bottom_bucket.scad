/*
FILE   : bottom_bucket.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-04
MODIFIED : 2022-05-17
MODIFIED : 2022-05-20
    
LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

//*/

$fn = 200;
bottom = 1; // thickness of the bottom
heigth = 45+bottom; // heigth of the piece
radius = 100; // radius of the bucket
support = 2; // thickness of the support
little_heigth = 10; // heigth covering the bucket
thickness = 1; // thickness of the wall
tap_hole = 21/2; // diametre / 2  of the tap hole
tap_fixer = 5; // space to fix the tap
n_side = 9; // choose the number of side for the inner hole
angle = 360/n_side; // angle interior
length = radius * 2*cos ((180-angle)/2); // length of each little support
translation = radius * sin((180-angle)/2); // translation of the cubes to cut all the interior
turn = n_side-1; // 0 to n_side-1 rotation for the cubes
slope = 10; // heigth of the slope

// Code: 
difference (){ 
    cylinder(heigth, r = radius+thickness); // body 
    translate ([0,0,heigth-little_heigth]) // bucket size
            cylinder(little_heigth, r = radius);
    for (i = [0:turn]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,heigth - little_heigth-support])
                cube ([length,radius,support]);
        }
    }
    translate ([0,0,bottom + slope]) // liquid location
            cylinder(heigth-little_heigth-support-slope, r = radius);
    rotate ([0,atan(slope/(2*radius)),0]) // slope
        translate ([0,0,bottom + slope/2])
            cylinder (slope, r1 = radius, r2 = radius - 1);
    translate ([radius-thickness,0,bottom+tap_hole+tap_fixer]) // hole for the tap
            rotate ([0,90,0])
                cylinder (thickness*2, r = tap_hole);
}