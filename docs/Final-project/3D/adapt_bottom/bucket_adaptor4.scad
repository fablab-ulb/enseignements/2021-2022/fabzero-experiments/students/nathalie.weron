/*
FILE   : bucket_adaptor4.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-16

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

//*/

$fn = 200;
rtop = 97.6; // (97.6) is perfect. calculate the radius exterior (!) of the bottom of the bucket with r = l/2*pi, l = 61.7 cm -> r = 98.2 mm but trying with (97.9) is nearly perfect, a little bit too big, 97.5 should be good, real r = 97.6.
// the bottom of the other bucket is l = 64.4 cm -> r = 102.5 mm but trying with (100.9+1) is perfect -> real r = r - 0.6.
rbottom = 103.5; // (103.5) is perfect but a bit tight so push a bit. calculate the radius exterior (!) of the bucket at the correct height (4.5cm) with r = l/2*pi, l = 65.4 cm -> r = 104.1 mm -> real r = 103.5 mm 
rmax = rbottom; // the body needs to be bigger than both buckets so the bigger radius is used
rmin = rtop; // the inner quadrilater needs to be adapted to the little radius
thickness = 1; // thickess of the external part of the adaptor
height = 22; // total height of the adaptor
half_height = 10; // height of the adaptor on both bucket -> 2 mm of thickness between the 2 buckets
n_side = 9; // choose the number of side for the inner hole
angle = 360/n_side; // angle interior
length = rmin * 2*cos ((180-angle)/2); // length of each little support
translation = rmin * sin((180-angle)/2); // translation of the cubes to cut all the interior
turn = n_side-1; // 0 to n_side-1 rotation for the cubes

/*
//to see if the holes file would touch the adaptor
diff = 7; 

translate ([0,0,half_height])
    cylinder (thickness, r = 97.9 - diff); // test for the holes disc

/*

//code test for the design:
difference () {
    cylinder(1, r = rmax + thickness); // body
    cylinder(0.4, r = rbottom); // bottom hole
    translate ([0,0,1-0.4]) // top hole
        cylinder(0.4, r = rtop);
        
    for (i = [0:turn]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,0.4])
                cube ([length,rmin,height-2*half_height]);
        }
    }
}

//*/


// Code:
difference () {
    hull (){
    cylinder(height-half_height, r = rbottom + thickness); // body bottom
    translate ([0,0,height-half_height])
        cylinder(half_height, r = rtop + thickness); // body top
    }
    cylinder(half_height, r = rbottom); // bottom hole
    translate ([0,0,height-half_height]) // top hole
        cylinder(half_height, r = rtop);
        
    for (i = [0:turn]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,half_height])
                cube ([length,rmin,height-2*half_height]);
        }
    }
}