/*
FILE   : bucket_adaptor5.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-16

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)..

//*/

// Too big to work

$fn = 200;
rtop = 104.3; // calculate the radius exterior (!) of the bottom of the bucket with r = l/2*pi, l = 65.9 cm -> r = 104.9 mm -> real r = 104.3 mm
rbottom = 105.1; // calculate the radius exterior (!) of the bucket with r = l/2*pi, l = 66.4 cm -> r = 105.7 mm -> real r = 105.1 mm 
rmax = rbottom; // the body needs to be bigger than both buckets so the bigger radius is used
rmin = rtop; // the inner quadrilater needs to be adapted to the little radius
thickness = 1; // thickess of the external part of the adaptor
height = 22; // total height of the adaptor
half_height = 10; // height of the adaptor on both bucket -> 2 mm of thickness between the 2 buckets
n_side = 9; // choose the number of side for the inner hole
angle = 360/n_side; // angle interior
length = rmin * 2*cos ((180-angle)/2); // length of each little support
translation = rmin * sin((180-angle)/2); // translation of the cubes to cut all the interior
turn = n_side-1; // 0 to n_side-1 rotation for the cubes


// Code:
difference () {
    hull (){
    cylinder(height-half_height, r = rbottom + thickness); // body bottom
    translate ([0,0,height-half_height])
        cylinder(half_height, r = rtop + thickness); // body top
    }
    cylinder(half_height, r = rbottom); // bottom hole
    translate ([0,0,height-half_height]) // top hole
        cylinder(half_height, r = rtop);
        
    for (i = [0:turn-1]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,half_height])
                cube ([length,rmin,height-2*half_height]);
        }
    }
    rotate ([0,0,turn*angle]) 
            translate ([-length/2,-translation*2,0])
                cube ([length,rmin*2,height]);
}