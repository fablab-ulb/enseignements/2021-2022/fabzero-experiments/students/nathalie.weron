/*
FILE   : lil_tap.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-27

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn = 200;
LL = 25;
lL = 15;
radius = 2.5;
Radius = 4;
diff = 0.5;


//Code: 
difference(){
    union (){
        cylinder (LL, r1 = radius, r2 = Radius);
        translate([0,0,LL/2])
            rotate([0,90,0])
                cylinder(lL, r = radius);
    }
    cylinder (LL, r1 = radius-diff, r2 = Radius-diff);
    translate([0,0,LL/2])
        rotate([0,90,0])
            cylinder(lL, r = radius-diff);
}
/*/

/*
//Code: to close the tap
cylinder (LL, r1 = radius, r2 = Radius);
/*/