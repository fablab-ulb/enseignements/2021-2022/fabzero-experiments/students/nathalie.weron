/*
FILE   : bucket_slope.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-07
MODIFIED : 2022-05-20

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

//*/

$fn = 200;
radius = 97.5; // (97.1) is a bit little but enough. calculate the radius interior (!) of the bucket (the slope needs to enter the bucket to the bottom) with r = l/2*pi, l = 61cm -> r = 97.1 mm
slope = 10; // height of the slope

/*
// radius test:
difference (){
    cylinder (1, r = radius, center = true);
    cylinder (1, r = radius-5, center = true);
}
//*/


//Code:
difference (){
    cylinder (slope, r = radius, center = true); // base
    rotate ([atan(slope/(2*radius)),0,0]) // cut for the slope
        translate ([0,0,slope/2])
            cylinder (slope, r1 = radius +1, r2 = radius, center = true);
}
//*/