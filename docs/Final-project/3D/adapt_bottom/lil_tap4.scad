/*
FILE   : lil_tap4.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-31

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn = 200;
L = 25; // total length of the body: the distance between the 2.5 mm radius and the 4 mm radius is 25 mm.
LL = 9.45; // length of the conic part of the body: (25 mm/2) - radius2
lL = 7; // lentgth of the side hole for the liquid to flow
radius = 3.5; // little radius of the body
Radius = 6; // big radius of the body, 3.067 mm radius 9.45 mm further than the little radius
diff = 0.5; // difference between the exterior and the interior of the body, 0.5 is the minimum to print
radius2 = 6; // needs to be the closest and below Radius
thick = 0.5; // thickness of the little stop
rint = 4.33; // little radius of the interior mecanism
Rint = 5.35; // big radius of the interior mecanism, 3.3 mm further than the 2.2 mm radius
lint = 7; // distance between the little and the big radius


//Code: 
difference(){ // exterior
    union (){
        cylinder(LL, r1 = radius, r2 = Radius);
        translate([0,0,15])
            rotate([0,90,0])
                cylinder(lL, r = radius2);
        translate([0,0,LL])
            cylinder(L-LL, r = Radius);
    }
    cylinder (LL, r1 = radius-diff, r2 = Radius-diff);
    translate([0,0,15])
        rotate([0,90,0])
            cylinder(lL, r = radius2-diff);
    translate([0,0,LL])
            cylinder(L-LL, r = Radius-diff);
}


difference(){ // little stop
    translate([0,0,L-thick])
        cylinder(thick, r = Radius);
    translate([-Radius,-Radius,L-thick])
        cube([Radius+0.25,Radius*2,thick]);
}

translate([0.0,0,5]){ // interior mecanism to close the tap
translate([0,0,3.33])
    cylinder (lint, r1 = rint, r2 = Rint);
difference(){
    translate([0,0,3.33+lint])
        cylinder (L, r = Rint-0.5);
    translate([0,-(Rint),L-(L-(lint+3.33))])
        cube([Rint,(Rint)*2,L-(lint+3)]); // L-(LL+3.33) normally -> L-(LL+3)
}
}