/*
FILE   : bucket_adaptor.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-07
MODIFIED:  2022-05-10
MODIFIED:  2022-05-11

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

//*/

$fn = 200;
rtop = 97.9; // calculate the radius exterior (!) of the bottom of the bucket with r = l/2*pi, l = 61.5 cm
rbottom = 100.9+1; // calculate the radius exterior (!) of the bucket at the correct heigth (+- 3.5cm for +- 1 litre) with r = l/2*pi, l = 63.4 cm
rmax = rbottom; // the body needs to be bigger than both buckets so the bigger radius is used
rmin = rtop; // the inner quadrilater needs to be adapted to the little radius
thickness = 1; // thickess of the external part of the adaptor
heigth = 22; // total heigth of the adaptor
half_heigth = 10; // heigth of the adaptor on both bucket -> 2 mm of thickness between the 2 buckets
n_side = 9; // choose the number of side for the inner hole
angle = 360/n_side; // angle interior
length = rmin * 2*cos ((180-angle)/2); // length of each little support
translation = rmin * sin((180-angle)/2); // translation of the cubes to cut all the interior
turn = n_side-1; // 0 to n_side-1 rotation for the cubes

/*
//to see if the holes file would touch the adaptor
diff = 7; 

translate ([0,0,half_heigth])
    cylinder (thickness, r = rtop - diff); // test for the holes disc



//code test for the design:
difference () {
    cylinder(1, r = rmax + thickness); // body
    cylinder(0.4, r = rbottom); // bottom hole
    translate ([0,0,1-0.4]) // top hole
        cylinder(0.4, r = rtop);
        
    for (i = [0:turn]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,0.4])
                cube ([length,rmin,heigth-2*half_heigth]);
        }
    }
}

//*/


// Code:
difference () {
    cylinder(heigth, r = rmax + thickness); // body
    cylinder(half_heigth, r = rbottom); // bottom hole
    translate ([0,0,heigth-half_heigth]) // top hole
        cylinder(half_heigth, r = rtop);
        
    for (i = [0:turn]){ // octogone hole   
        rotate ([0,0,i*angle]) {
            translate ([-length/2,-translation,half_heigth])
                cube ([length,rmin,heigth-2*half_heigth]);
        }
    }
}