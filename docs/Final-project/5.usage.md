# 5. The practical application             

## Table of contents:          
[1.The principle](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#1the-principle)                   

[2.What would go in my compost ?](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#2what-would-go-in-my-compost)               

[3.The cover materials](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#3the-cover-materials)               

[4.Filling up](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#4filling-up)           

[5.Temperatures](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#5temperatures)            

[6.Vermicomposting](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#6vermicomposting)           

[7.Cleaning](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#7cleaning)           

[8.The slope test](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#8the-slope-test)     

[9.Improving the process](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#9improving-the-process)             
[9.1.Good things](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#91good-things)             
[9.2.Things to improve](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#92things-to-improve)            
[9.3.Things to remove](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#93things-to-remove)            

[10.The inutility of my project](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/#10the-inutility-of-my-project)           

## 1.The principle               
The idea was to make a thermophilic compost in the bucket during maybe 2-3 weeks then to add it in a tower of vermicompost buckets for 6 months BUT I would need a hell lot of buckets and there could be contaminations from new compost buckets to old compost buckets. Therefore, the idea changed to:              

1. Make a thermophilic compost in the toilet bucket with the help of the compost thermometre.               

2. Once the compost thermometre LED is green, empty the bucket in a big vermicomposting bin that will rest for at least 6 months: the big vermicomposting bin needs to be big enough for the volume of humanure produced in 6 months by the household, or better, by a building. After 6 months of filling, the bins are full and rest for 6 months. There would be only 2 big vermicomposting bins instead of at least 26 little buckets (one per week during 6 months).            

## 2.What would go in my compost ?       

* The basis: urine, poop, toilet paper, some menstrual blood (yes it exists, sorry not sorry).

* The bathroom extras: hairs, clipping nails, paper cotton-tipped swab, paper tissues, compostable dental floss (easy as the toilet is in my bathroom).

* Kitchen related: foods that will not go in our compost because my parents do not want to (based on ...), cooking water from pasta or cans liquid if needed for moisture.

* A cover material: hemp litter, 5€ for 3 kg.

## 3.The cover materials                       
I used to have a guinea pig and he had hemp litter. I choose it because it has no dust and is smooth to the touch. As for myself, I think that it would be a nice litter that will not need a cup or glove to be held barehanded.           
On the packaging it is written that the hemp litter is 100% natural, has a very high absorbency, is compostable and dust free. It is said to be ecological because its cultivation did not use any fertilisers or pesticides.

If I continue to use my dry toilet I could search a sawdust source in a carpenter or someone like that to recycle their waste and not to rely on hemp production.

I did not used a coarse material to trap air but I would have used grass clipping from my garden. If I did not had a garden I would have used straw as it is a by-product of cereal production and hay is difficult enough to made to be restricted to the feed of animals.

## 4.Filling up             
Before using the bucket: fill nearly 5 cm of it with litter.              
**Update**: because of the volume of urine, more is better.             
Filling the bucket adds more weight than volume because excreta fill up the interstitial spaces. The bucket ends up filled with litter and just a little bit of volume is used by fecal matters. Urine is absorbed by the litter.                  

My less than 10 litres bucket was full in 48 hours (start: 25/05 evening, end: 27/05 evening) with myself as unique user. I went once to another toilet. It has only liquid and solid dejections, toilet paper and some hair, compostable dental floss and paper cotton-tipped swab. Urine was enough to moisturize as I needed to empty the bottom tank into the bucket several times to stop liquid percolating in it.     

My 3 kg hemp litter bag was nearly full: 2.4 kg. I forgot to weight it before opening it. I forgot to weight the bucket before get it ready for temperature monitoring.              
I think that I would need a new bag every 10 days ...         

## 5.Temperatures          
You can rely on the compost thermometre to monitor the thermophilic stage.       

My monitoring start at 23°C (27/05 evening) in a room with an ambient temperature of 20°C (result of the thermometre during code tests).
I had 24°C hours later.

The first 9V battery was dead after less than 24 hours ... I will manually start the thermometre to see the temperature then turn it off.             
I put the thermometre on sector using the USB cable.           

![thermometre on sector](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/thermometre_sector.jpg)      

I had 23°C on the 28/05 then 22°C in the evening -> I tried to add cooking + fresh cream cleaning water and vegetables peels (zucchini and carrots) to boost the heat.          
Before adding them I observe correct moisture on the top and a little smell of ammoniac in the top centre.        

![lid moisture](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/lid.jpg)         

The result was 22°C in the next morning then 23°C at noon until the evening. Back to 22°C in the next morning.         

I tried to close the blanket and the towel more.

On the 01/06/2022, the temperature decreased to 21°C in a 19°C ambient room.              
The lid was moist. I inserted a stick in the compost to open ways for oxygen in the depths. I checked the bottom tank and there was liquid. I put it back in the bucket using a bigger bucket to put the compost bucket during the transfer. I should have open a hole to pour the liquid instead of pouring directly over the content.            
And I changed the towel for a blanket. Maybe it will keep the heat better.

On the 02/06/2022 morning, 22°C.              
I saw on my computer that it was nearly 22.1°C then later 22.5°C.              
The next morning it is still 22.5°C then 22.8°C so it is heating slowly.               
I mixed the content how I could and I added foodscraps (cheese, mushroom, green beans, apple, onion) to tried to boost the heat again.             
Result: 23.5°C in the evening then 23.8°C on the 04/06/2022 morning and 24.5°C in the evening. Same in the next morning, 25°C in the evening.                    
Slightly less on the 06/06/2022 morning so I mixed again but deeper (with gloves).                  
Result: the temperature decreased a bit (until 24,87°C) then increased again. It was 25.25°C in the afternoon but then it decreased until 23.7°C in the afternoon of the 07/06/2022.

I stopped my attempt to obtain high temperatures here.

The temperature decreased to 22°C but not below then increased again to 23.25°C on the 11/06/2022. Then I stopped to check.                   

**Conclusion**: I badly filled the bucket with only hemp litter, not much of solid excreta and a lot of urine. The bucket lacked oxygen from a coarse material so it needed mixing, and it lacked foodscraps to help the heating process. Maybe the insulation is not working as well and/or prevent enough oxygen to enter the bucket.         

**Update**: On the first of July, the temperature was 24.75°C ! I opened the bucket to see the advancement: a relative high amount of humidity on the lid, nearly no smell compared to before, less volume in the bucket, a very little bit of something like rot or fungi on the thermometre probe and a small volume of liquid in the tank. I removed the "Norwegian kettle" to see the temperature without before adding the worms.              
 The temperature decreased to 23°C then increased to 24°C. Maybe the insulation is not working ?

**New update**: 17/08/2022, the temperature is 25°C but it could be all because of the hot weather. It smells when the lid is open. It is still wet, nearly no liquid in the tank and the moisture of the content seems right. The colour of the content seems darker and there are some cocoons, some brown and some black with bits of hairs, probably of flies. I saw a white larvae but it is difficult to find. Maybe there are just deeper. They are very small. There are cocoons on the lid too. They could have enter because of the small opening due to the thermometre. I do not know if it is a problem because I do not have a fly infestation in this room, no flies are staying flying around the bucket. The bucket with the termometre is 4.0 kg.                                   
On the 25/08/2022, flies were coming out of the bucket so I remove the thermometre that is not useful anymore to better close the lid. I also add some dry grass on top to act as a natural lid and once a day I put the bucket outside to open the lid and let the flies out.

## 6.Vermicomposting             
I did not have the time to start the vermicomposting but if I do it later, I would explain it here.     

**Update**: I added the worms on the 23/09/2022. I paid 7€50 for 250g in a fishing shop. They were stocked in a fridge. I put them in the top of the bucket, under some dry grass and a bit of cleaning paper (used to wipe drops of the bucket). Next day, some seem fine and others dead ... Maybe they were hurted during my transfert ? They create a biologic white glue and have bigger parts. The odour is really reduced in one day.                     
On the 27/09, all the worms were dead ... Maybe because of the ammoniac of the bucket, maybe because purchase in a fish shop is not a good idea.


## 7.Cleaning              
In a classic dry toilet, there is no toilet slab to clean and there should not have spillage to clean either.           
You just have to clean the bucket once emptied.          

I cannot explain how I cleaned my bucket as not enough time has passed to empty but I can explain how it should be cleaned:             
Use water (rainwater or greywater is good), soap (it can be biodegradable) and a long-handled toilet brush. The cleaning water can be returned to another compost to not contaminate the ground or the sewers.      

After 10 years of usage, plastic buckets could still have bad smell after being cleaned so they are often changed after this period. Inox buckets have therefore a better durability.        

## 8.The slope test                  
The bucket that I tested had no tap or slope so I tested the slope separately.             

The slope allows to empty a bit more from the tank:          

![max water](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/max_water.jpg)             
This is the water remaining in the tank without slope.              

![water strip](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/water_strip.jpg)                
This is the water that can be emptied with the slope.

A bigger slope or another kind of filling would help to empty more but some water is stuck because of the size of the tap.         

![min water](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/min_strip.jpg)               
In the glass there is the water emptied after the maximal emptying.


## 9.Improving the process             
To have the prototype ready for the presentation, I did not used the bucket with a tap for the tests so I cannot judge the efficiency of the tap nor the slope.         
I can only discussed what I really tried: everything else except the lil_tap that I did later.       

### 9.1.Good things                
The pierced bottom and the tank is a good idea to ensure anaerobic conditions in the bottom of the bucket.

The adaptor is working well enough and an inox version like describe in the [4. Prototyping](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/4.Prototyping/#23now-what-) page would work even better.

### 9.2.Things to improve            

The insulation could be sew and cut to the dimension of the bucket to be placed faster and have a better aesthetic.          

The thermometre could be put in a nice box to look better and be easier to move.          

As a lot of urine is excreted compared to fecal matter, urine diversion should be used to be able to fill the bucket with more fecal matter in the same volume of litter. The time to fill the bucket would be longer as well and food could be easily added.                            
Urine would be added during the composting process to provide nitrogen and humidity.     

Maybe the volume of a 20 litres bucket is not enough to easily attain correct temperatures and it would be easier to start a real compost at the building scale and to empty all buckets in it while it is hot and not full. Then, when full and the temperatures decreased enough, earthworms could be added directly in it for 6 months of vermicomposting. Of course, this proposition should be tested to see if it is working and if the duration of the vermicomposting is enough, as well as the duration of the entire process from starting the hot compost to the end of vermicomposting.         
I think that the parking or the basement of buildings could be used.          

The holes circle was not tightly screwed so it was not against the bottom of the bucket and the 8 mm holes were not reduced to 2 mm to prevent earthworms to drawn in the bottom tank liquid. However, it not appeared to be a problem for the composting part and I did not try the vermicomposting at this time. It is possible to just be more careful when screwing the holes circle but ...      

![holes circle problem](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/-/raw/main/docs/Final-project/Images/5.Usage/holes_circle_prob.jpg)             

### 9.3.Things to remove                 
... the holes disc is not necessary in my second though of the process as the bucket would not actually be used for the vermicomposting and the bucket would just have 2 mm holes.     

## 10.The inutility of my project                          
The problem of my idea is that I focus on urban people that do not have a garden, to produce a fertiliser that they will not use and probably do not care about. Therefore they could prefer to pay someone else to do it somewhere else (like now with the treatment of blackwater). My project is not possible for the moment because the fertiliser produced could not be put in the market nor used by food producer until regulations are made for that. However, once made, a service could be put in place to do it for urban people. Therefore my project is not really useful.                 

This new service could nonetheless be inspired by my process and the improvements proposed above:                  
A urine reservoir of 10 litres and an inox bucket of 22 litres with a lid (like the one proposed in the [4. Prototyping](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/4.Prototyping/#23now-what-) page would be great. The volumes are based on [the Trobolo urine-diverting dry toilet kit](https://trobolo.com/fr/produits/kit-diy/). I would advise to make a rotation with 3 buckets and to give new clean buckets already filled with cover material. Then 1 bucket could being filled in the toilet, 1 bucket would being emptied of cover material and will be the next toilet bucket and 1 bucket would either be filled of fecal matter and litter, waiting to be picked up, or clean, full of cover material and ready to be emptied.               

Like Maxime told me, it would be better to change the sanitation infrastructure in buildings and if the vermicompost is shared, why the first step would not be shared as well ? So maybe my idea of a little bucket could become a big bin filled directly by the toilets of all the households of one building then collected by a service or composted on-site with a bit of the urine from the urine container located next to it. The rest of the urine could be collected for valorisation in high-value products.            
If composted on-site, test is needed to see if the composting would be thermophilic or mesophilic, impacting the duration of the total process and the moment when earthworms would be added.
