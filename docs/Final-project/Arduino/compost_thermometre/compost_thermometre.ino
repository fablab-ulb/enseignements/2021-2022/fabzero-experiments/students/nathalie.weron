
/*
   FILE   : compost_thermometre

   AUTHOR : Nathalie Wéron

   DATE   : 2022-05-27

   LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

   BASED ON :
    Blink
    modified 8 May 2014
    by Scott Fitzgerald
    modified 2 Sep 2016
    by Arturo Guadalupi
    modified 8 Sep 2016
    by Colby Newman
    This example code is in the public domain.
    https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
   AND
    https://www.makerguides.com/ds18b20-arduino-tutorial/ for the thermometer code
    COPYRIGHT © 2021 MAKERGUIDES.COM
   AND
     for the 4-Digit 7-Segment Display
     https://www.makerguides.com/tm1637-arduino-tutorial/
   AND
     for the sleep mode while working
     https://circuitdigest.com/microcontroller-projects/arduino-sleep-modes-and-how-to-use-them-to-reduce-power-consumption
*/

#include <LowPower.h>

#define LEDg1          3 // green LED 1 to say that the thermophilic treatment is done with Tobj1
#define LEDg2          4 // green LED 2 to say that the thermophilic treatment is done with Tobj2
#define LEDg3          5 // green LED 3 to say that the thermophilic treatment is done with Tobj3
#define LEDr           7 // red LED for Tmax
int Tobj1 = 46;  // 46°C is needed for 1 week time so 7*24*3600 = 604 800 seconds so t1 = 60 480
long t01 = 60480; // time for Tobj1: seconds / 10, 32 767 is the max value for int for arduino Uno -> long can go to 2 147 483 647
int Tobj2 = 50;  // 50°C is needed for 1 days so 24*3600 = 86 400 seconds so t2 = 8 640
int t02 = 8640; // time for Tobj2
int Tobj3 = 62;  // 62°C is needed for 1 hour time so 3600 seconds so t3 = 360
int t03 = 360; // time for Tobj3
int Tmax = 65;  // the compost should not have a temperature above 65°C
int time1 = -1; // start the count for Tobj1
int time2 = -1; // start the count for Tobj2
int time3 = -1; // start the count for Tobj3

#include <OneWire.h> // include both libraries
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 8 // define the thermometre pin
OneWire oneWire(ONE_WIRE_BUS); // Create a new instance of the oneWire class to communicate with any OneWire device
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to DallasTemperature library

#include <TM1637Display.h> // include the library
#define DIO 9
#define CLK 10
TM1637Display display = TM1637Display(CLK, DIO); // Create display object of type TM1637Display


void setup() {
  Serial.begin(9600); // initialize serial communication at 9600 bits per second

  pinMode(LEDg1, OUTPUT); // set the LEDs as output
  pinMode(LEDg2, OUTPUT);
  pinMode(LEDg3, OUTPUT);
  pinMode(LEDr, OUTPUT);

  sensors.begin(); // start up the library
}

void loop() {
  int t0 = millis(); // start a counter for the time between 2 points
  sensors.requestTemperatures(); // Send the command for all devices on the bus to perform a temperature conversion
  float tempC = sensors.getTempCByIndex(0); // Fetch the temperature in degrees Celsius for device index, the index 0 refers to the first device

  // print the temperature in the Serial Monitor:                        
  Serial.print("Temperature: ");
  Serial.print(tempC);   // print the temperature in Celsius
  Serial.println("°C");  // println to say it is the end of the line

  display.setBrightness(1); // Set the brightness: not a huge difference between 1 and 10
  display.showNumberDecEx(tempC); // display the temperature

  if (tempC >= Tobj1) {
    time1 = time1 + 1; // count 1 turn
  }
  else {
    time1 = -1; // reset time1
  }

  if (tempC >= Tobj2) {
    time2 = time2 + 1; // count 1 turn
  }
  else {
    time2 = -1; // reset time2
  }

  if (tempC >= Tobj3) {
    time3 = time3 + 1; // count 1 turn
  }
  else {
    time3 = -1; // reset time3
  }

  if (time1 >= t01) { // to say that 46°C has been reached for 1 week
    digitalWrite(LEDg1, HIGH);   // turn the LEDg1 on (HIGH is the voltage level)
  }
  if (time2 >= t02) { // to say that 50°C has been reached for 1 day
    digitalWrite(LEDg2, HIGH);   // turn the LEDg2 on (HIGH is the voltage level)
  }
  if (time3 >= t03) { // to say that 60°C has been reached for 1 hour
    digitalWrite(LEDg3, HIGH);   // turn the LEDg3 on (HIGH is the voltage level)
  }

  if (tempC >= Tmax) { //the compost has been to hot to be safe
    digitalWrite(LEDr, HIGH);   // turn the red LED on (HIGH is the voltage level)
  }

  Serial.println(time1);
  
  delay (0); // no delay is needed, this code seems to make 1 turn in 10 seconds
  
  int t1 = millis (); //
  Serial.println(t1 - t0); // measures the time passed between t0 and t1

  Serial.println("Nap");
  delay(25); // to give time to print the above, with 25 ms this code seems to make 1 turn in 10 seconds
  LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF,
                SPI_OFF, USART0_OFF, TWI_OFF);
                
  Serial.println("Woke up");
  
  int t2 = millis (); 
  Serial.println(t2 - t0); // measures the time passed between t0 and t2
}
