
/*
   FILE   : test_compost_thermometre8

   AUTHOR : Nathalie Wéron

   DATE   : 2022-05-25

   LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

   BASED ON :
    Blink
    modified 8 May 2014
    by Scott Fitzgerald
    modified 2 Sep 2016
    by Arturo Guadalupi
    modified 8 Sep 2016
    by Colby Newman
    This example code is in the public domain.
    https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
   AND
    https://www.makerguides.com/ds18b20-arduino-tutorial/ for the thermometer code
    COPYRIGHT © 2021 MAKERGUIDES.COM
   AND
     A code for the button use created by ArduinoGetStarted.com
     This example code is in the public domain
     Tutorial page: https://arduinogetstarted.com/tutorials/arduino-button
   AND
     for the interruption
     https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
   AND
     for the 4-Digit 7-Segment Display
     https://www.makerguides.com/tm1637-arduino-tutorial/

     sleep mode: https://thekurks.net/blog/2018/1/24/guide-to-arduino-sleep-mode
*/


#include <avr/sleep.h> // the AVR library contains the methods that controls the sleep modes

const int BUTTON_PIN = 2; // the number of the pushbutton pin
volatile int action = false;   // the starting value of action to start with a shut down thermometre

#define LEDg1          3 // green LED 1 to say that the thermophilic treatment is done with Tobj1
#define LEDg2          4 // green LED 2 to say that the thermophilic treatment is done with Tobj2
#define LEDg3          5 // green LED 3 to say that the thermophilic treatment is done with Tobj3
#define LEDr           7 // red LED for Tmax
int Tobj1 = 23;  // 46°C is needed for 1 week time so 7*24*3600 = 604 800 seconds
int t1 = 10; // time in second for Tobj1
int Tobj2 = 25;  // 50°C is needed for 1 days so 24*3600 = 86 400 seconds
int t2 = 5; // time in second for Tobj2
int Tobj3 = 27;  // 62°C is needed for 1 hour time so 3600 seconds
int t3 = 2; // time in second for Tobj3
int Tmax = 30;  // The compost should not have a temperature above 65°C
int time1 = 0; // start the count for Tobj1
int time2 = 0; // start the count for Tobj2
int time3 = 0; // start the count for Tobj3

#include <OneWire.h> // include both libraries
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 8 // define the thermometre pin
OneWire oneWire(ONE_WIRE_BUS); // Create a new instance of the oneWire class to communicate with any OneWire device
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to DallasTemperature library

#include <TM1637Display.h> // include the library
#define DIO 9
#define CLK 10
TM1637Display display = TM1637Display(CLK, DIO); // Create display object of type TM1637Display


void setup() {
  Serial.begin(9600); // initialize serial communication at 9600 bits per second

  pinMode(LEDg1, OUTPUT); // set the LEDs as output
  pinMode(LEDg2, OUTPUT);
  pinMode(LEDg3, OUTPUT);
  pinMode(LEDr, OUTPUT);

  pinMode(BUTTON_PIN, INPUT_PULLUP);// the pull-up input pin will be HIGH when the switch is open and LOW when the switch is closed
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), change, FALLING); // the interuption of the loop by the change function will be made when the button is "falling": when the pin goes from HIGH to LOW

  sensors.begin(); // start up the library
}

void change() {
  action = !action; // change the sign of action: TRUE -> FALSE, FALSE -> TRUE
  Serial.println("The state changed"); //  CAN BE REMOVED FOR FINAL
  delay (100);
}

void going_to_sleep() {
  sleep_enable(); // enabling sleep mode
  attachInterrupt (0, change, FALLING); // attaching an interrupt to the button
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // setting the sleep mode as full sleep
  sleep_cpu(); // activating sleep mode
}

void loop() {
  if (action == true) { // activation of the code
    sleep_disable(); // disable the sleep mode

    int t0 = millis();  // CAN BE REMOVED FOR FINAL
    sensors.requestTemperatures(); // Send the command for all devices on the bus to perform a temperature conversion
    float tempC = sensors.getTempCByIndex(0); // Fetch the temperature in degrees Celsius for device index, the index 0 refers to the first device

    // print the temperature in the Serial Monitor:                           CAN BE REMOVED FOR FINAL
    Serial.print("Temperature: ");
    Serial.print(tempC);   // print the temperature in Celsius
    Serial.println("°C");  // println to say it is the end of the line

    display.setBrightness(4); // Set the brightness:
    display.showNumberDecEx(tempC); // display the temperature

    if (tempC >= Tobj1) {
      time1 = time1 + 1; // count 1 second
    }
    else {
      time1 = 0; // reset time1
    }

    if (tempC >= Tobj2) {
      time2 = time2 + 1; // count 1 second
    }
    else {
      time2 = 0; // reset time2
    }

    if (tempC >= Tobj3) {
      time3 = time3 + 1; // count 1 second
    }
    else {
      time3 = 0; // reset time3
    }

    if ((time1 >= t1) || (time2 >= t2) || (time3 >= t3)) { // turn on the green light to say that the compost has done a thermophilic composting process
      if (time1 >= t1) { // to say that 46°C has been reached for 1 week
        digitalWrite(LEDg1, HIGH);   // turn the LEDg1 on (HIGH is the voltage level)
      }
      if (time2 >= t2) { // to say that 50°C has been reached for 1 day
        digitalWrite(LEDg2, HIGH);   // turn the LEDg2 on (HIGH is the voltage level)
      }
      if (time3 >= t3) { // to say that 60°C has been reached for 1 hour
        digitalWrite(LEDg3, HIGH);   // turn the LEDg3 on (HIGH is the voltage level)
      }
    }

    if (tempC >= Tmax) { //the compost has been to hot to be safe
      digitalWrite(LEDr, HIGH);   // turn the red LED on (HIGH is the voltage level)
    }

    Serial.println(time1);  // CAN BE REMOVED FOR FINAL
    int t1 = millis (); // CAN BE REMOVED FOR FINAL
    Serial.println(t1 - t0); // CAN BE REMOVED FOR FINAL
    delay (276); // because one loop takes 724 milliseconds + 276 milliseconds = 1 second
  }

  if (action == false) { // reset the code
    digitalWrite(LEDg1, LOW);    // reset the LEDs
    digitalWrite(LEDg2, LOW);
    digitalWrite(LEDg3, LOW);
    digitalWrite(LEDr, LOW);
    time1 = 0; // reset the times
    time2 = 0;
    time3 = 0;
    display.clear(); // reset the display
    Serial.println("cleared"); // CAN BE REMOVED FOR FINAL

    Serial.println("going to sleep"); // CAN BE REMOVED FOR FINAL
    delay (200);
    going_to_sleep();
  }
}
