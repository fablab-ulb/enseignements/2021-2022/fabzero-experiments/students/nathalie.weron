
/*
   FILE   : test_compost_thermometre2

   AUTHOR : Nathalie Wéron

   DATE   : 2022-05-03
   MODIFIED : 2022-05-19

   LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

   BASED ON :
    Blink
    modified 8 May 2014
    by Scott Fitzgerald
    modified 2 Sep 2016
    by Arturo Guadalupi
    modified 8 Sep 2016
    by Colby Newman
    This example code is in the public domain.
    https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
   AND
    https://www.makerguides.com/ds18b20-arduino-tutorial/ for the thermometre code
    COPYRIGHT © 2021 MAKERGUIDES.COM
*/

#define LEDo1          3 // orange LED 1 for Tobj1
#define LEDg           6 // green LED to say that the thermophilic treatment is done
#define LEDr           7 // red LED for Tmax
int Tobj1 = 23;  // 46°C is needed for 1 week time so 7*24*3600 = 604 800 seconds
int t1 = 3; // time in second for Tobj1
int Tmax = 30;  // The compost should not have a temperature above 65°C
unsigned long time1;  // start time1

#include <OneWire.h> // include both libraries
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 8 // define the thermometre pin
OneWire oneWire(ONE_WIRE_BUS); // Create a new instance of the oneWire class to communicate with any OneWire device
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to DallasTemperature library

void setup() {
  Serial.begin(9600); // initialize serial communication at 9600 bits per second

  pinMode(LEDg, OUTPUT); // set the LEDs as output
  pinMode(LEDo1, OUTPUT);
  pinMode(LEDr, OUTPUT);

  sensors.begin(); // start up the library
}

void loop() {
  int t0 = millis();  // CAN BE REMOVED FOR FINAL
  sensors.requestTemperatures(); // Send the command for all devices on the bus to perform a temperature conversion:
  float tempC = sensors.getTempCByIndex(0); // Fetch the temperature in degrees Celsius for device index, the index 0 refers to the first device

  // print the temperature in the Serial Monitor:                           CAN BE REMOVED FOR FINAL
  Serial.print("Temperature: ");
  Serial.print(tempC);   // print the temperature in Celsius
  Serial.println("°C");  // println to say it is the end of the line

  if (tempC >= Tobj1) {
    digitalWrite(LEDo1, HIGH);   // turn the LEDo1 on (HIGH is the voltage level)
    time1 = time1 + 1; // count 1 second
  }
  else {
    digitalWrite(LEDo1, LOW);    // turn the LEDo1 off by making the voltage LOW
    time1 = 0; // reset time1
  }

  if (time1 >= t1) { // turn on the green light to say that the compost has done a thermophilic composting process
    digitalWrite(LEDg, HIGH);   // turn the green LED on (HIGH is the voltage level)
  }

  if (tempC >= Tmax) { //the compost has been to hot to be safe
    digitalWrite(LEDr, HIGH);   // turn the red LED on (HIGH is the voltage level)
  }
  Serial.println(time1); // CAN BE REMOVED FOR FINAL
  int t1 = millis (); // CAN BE REMOVED FOR FINAL
    Serial.println(t1 - t0); // CAN BE REMOVED FOR FINAL
  delay (1000);
}
