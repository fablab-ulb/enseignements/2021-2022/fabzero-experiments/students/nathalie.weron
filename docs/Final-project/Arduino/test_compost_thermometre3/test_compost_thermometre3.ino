
/*
   FILE   : test_compost_thermometre3

   AUTHOR : Nathalie Wéron

   DATE   : 2022-05-03
   MODIFIED : 2022-05-19

   LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

   BASED ON :
    Blink
    modified 8 May 2014
    by Scott Fitzgerald
    modified 2 Sep 2016
    by Arturo Guadalupi
    modified 8 Sep 2016
    by Colby Newman
    This example code is in the public domain.
    https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
   AND
    https://www.makerguides.com/ds18b20-arduino-tutorial/ for the thermometre code
    COPYRIGHT © 2021 MAKERGUIDES.COM
*/

#define LEDo1          3 // orange LED 1 for Tobj1
#define LEDo2          4 // orange LED 2 for Tobj2
#define LEDo3          5 // orange LED 3 for Tobj3
#define LEDg           6 // green LED to say that the thermophilic treatment is done
#define LEDr           7 // red LED for Tmax
int Tobj1 = 23;  // 46°C is needed for 1 week time so 7*24*3600 = 604 800 seconds
int t1 = 10; // time in second for Tobj1
int Tobj2 = 25;  // 50°C is needed for 1 days so 24*3600 = 86 400 seconds
int t2 = 5; // time in second for Tobj2
int Tobj3 = 27;  // 62°C is needed for 1 hour time so 3600 seconds
int t3 = 2; // time in second for Tobj3
int Tmax = 30;  // The compost should not have a temperature above 65°C
int time1 = 0; // start the count for Tobj1
int time2 = 0; // start the count for Tobj2
int time3 = 0; // start the count for Tobj3

#include <OneWire.h> // include both libraries
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 8 // define the thermometre pin
OneWire oneWire(ONE_WIRE_BUS); // Create a new instance of the oneWire class to communicate with any OneWire device
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to DallasTemperature library

void setup() {
  Serial.begin(9600); // initialize serial communication at 9600 bits per second

  pinMode(LEDg, OUTPUT); // set the LEDs as output
  pinMode(LEDo1, OUTPUT);
  pinMode(LEDo2, OUTPUT);
  pinMode(LEDo3, OUTPUT);
  pinMode(LEDr, OUTPUT);

  digitalWrite(LEDg, HIGH); // to show that the thermometre is turned on
  delay(1000);
  digitalWrite(LEDg, LOW);

  sensors.begin(); // start up the library
}

void loop() {
  sensors.requestTemperatures(); // Send the command for all devices on the bus to perform a temperature conversion:
  float tempC = sensors.getTempCByIndex(0); // Fetch the temperature in degrees Celsius for device index, the index 0 refers to the first device

  // print the temperature in the Serial Monitor:                           CAN BE REMOVED FOR FINAL
  Serial.print("Temperature: ");
  Serial.print(tempC);   // print the temperature in Celsius
  Serial.println("°C");  // println to say it is the end of the line

  if (tempC >= Tobj1) {
    digitalWrite(LEDo1, HIGH);   // turn the LEDo1 on (HIGH is the voltage level)
    time1 = time1 + 1; // count 1 second
  }
  else {
    digitalWrite(LEDo1, LOW);    // turn the LEDo1 off by making the voltage LOW
    time1 = 0; // reset time1
  }

  if (tempC >= Tobj2) {
    digitalWrite(LEDo2, HIGH);   // turn the LEDo2 on (HIGH is the voltage level)
    time2 = time2 + 1; // count 1 second
  }
  else {
    digitalWrite(LEDo2, LOW);    // turn the LEDo2 off by making the voltage LOW
    time2 = 0; // reset time2
  }

  if (tempC >= Tobj3) {
    digitalWrite(LEDo3, HIGH);   // turn the LEDo3 on (HIGH is the voltage level)
    time3 = time3 + 1; // count 1 second
  }
  else {
    digitalWrite(LEDo3, LOW);    // turn the LEDo3 off by making the voltage LOW
    time3 = 0; // reset time3
  }

  if ((time1 >= t1) || (time2 >= t2) || (time3 >= t3)) { // turn on the green light to say that the compost has done a thermophilic composting process
    digitalWrite(LEDg, HIGH);   // turn the green LED on (HIGH is the voltage level)
  }

  if (tempC >= Tmax) { //the compost has been to hot to be safe
    digitalWrite(LEDr, HIGH);   // turn the red LED on (HIGH is the voltage level)
  }
  Serial.println(time1); // CAN BE REMOVED FOR FINAL
  delay (1000);
}
