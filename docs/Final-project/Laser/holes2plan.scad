/*
FILE   : holes2plan.scad
    
AUTHOR : Nathalie Wéron
    
DATE   : 2022-05-11
MODIFIED : 2022-05-20

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
//*/

$fn = 200;
rtop = 97.9; // radius of the bucket
diff = 7; // distance between the bucket exterior and the holes disc exterior
rholes = 1; // 2mm of diametre
rcenter = 1.5; // for screw with a 3 mm diametre
distance = 40; // distance between the holes centres
rholes8 = 4; // 8 mm of diametre
n = 6; // number of holes lines
angle = 360/n; // angle for the rotations

// Code: 
difference (){
    circle (r = rtop - diff); // disc
    circle (r = rcenter); // center hole for the attach of the disc to the bucket
    for (i = [0:n]){ // 2 mm holes
        rotate ([0,0,i*angle*2]) { // space
            translate ([0.5*distance,0,0]) 
                circle (r = rholes);
        }
        rotate ([0,0,i*angle]) { // space
            translate ([1.5*distance,0,0]) 
                circle (r = rholes);
            rotate ([0,0,angle/2]){ // delay
                translate ([distance,0,0])
                    circle (r = rholes);
                translate ([2*distance,0,0])
                circle (r = rholes);
            }
        }
    }
    for (i = [1:n]){ // 8 mm holes
        rotate ([0,0,i*angle*2]) { // space
            rotate ([0,0,angle/2]){ // delay
                translate ([0.5*distance,0,0]) 
                    circle (r = rholes8);
                }
            }
        rotate ([0,0,i*angle]) { // space
            translate ([distance,0,0])
                circle(r = rholes8);
            translate ([2*distance,0,0])
                circle (r = rholes8);
            rotate ([0,0,angle/2]){ // delay
                translate ([1.5*distance,0,0]) 
                    circle(r = rholes8);
            }
        }
    }
}